import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'
import authService from '@/services/auth'
import { useMessageStore } from './message'
import { useRoute, useRouter } from 'vue-router'
import { useLoadingStore } from './loading'

export const useAuthStore = defineStore('auth', () => {
  const messageStore = useMessageStore()

  const login = async (email: string, password: string) => {
    loadingStore.doLoad()
    try {
      const res = await authService.login(email, password)
      console.log(res.data)
      localStorage.setItem('user', JSON.stringify(res.data.user))
      localStorage.setItem('access_token', JSON.stringify(res.data.access_token))
      messageStore.showMessage("Login Success")
      router.push('/')
    } catch (e: any) {
      console.log(e.message)
      messageStore.showMessage(e.message)
    }
    loadingStore.finish()

  }
  const router = useRouter()
  const loadingStore = useLoadingStore()
  const logout = () => {
    localStorage.removeItem('user')
    localStorage.removeItem('access_token')
    router.replace('/login')
  }
  function getCurrentUser(): User | null {
    const strUser = localStorage.getItem('user')
    if (strUser !== null) {
      return JSON.parse(strUser)
    } else {
      return null
    }
  }

  function getToken(): string | null {
    const strToken = localStorage.getItem('access_token')
    if (strToken !== null) {
      return JSON.parse(strToken)
    } else {
      return null
    }
  }

  return { getCurrentUser, login, getToken, logout }
})
