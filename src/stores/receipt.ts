import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Receipt } from '@/types/Receipt'
import { useAuthStore } from './auth'

export const useReceiptStore = defineStore('receipt', () => {
  const authStore = useAuthStore()
  const receiptItems = ref<ReceiptItem[]>([])
  // const receipt = ref<Receipt>({
  //   id: 0,
  //   createdDate: new Date(),
  //   total: 0,
  //   amount: 0,
  //   change: 0,
  //   paymentType: '',
  //   userId: authStore.getCurrentUser().id,
  //   memberId: -1,
  //   user: authStore.getCurrentUser()
  // })

  const calReceipt = function () {
    console.log('Cal')
  }

  const addReceiptItem = (newReceiptItem: ReceiptItem) => {
    receiptItems.value.push(newReceiptItem)
    calReceipt()
  }
  const deleteReceiptItem = (selectedItem: ReceiptItem) => {
    const index = receiptItems.value.findIndex((item) => item === selectedItem)
    receiptItems.value.splice(index, 1)
    calReceipt()
  }
  const incUnitOfReceiptItem = (selectedItem: ReceiptItem) => {
    selectedItem.unit++
    calReceipt()
  }
  const decUnitOfReceiptItem = (selectedItem: ReceiptItem) => {
    selectedItem.unit--
    if (selectedItem.unit === 0) {
      deleteReceiptItem(selectedItem)
    }
    calReceipt()
  }
  const removeItem = (item: ReceiptItem) => {
    const index = receiptItems.value.findIndex((ri) => ri === item)
    receiptItems.value.splice(index, 1)
    calReceipt()
  }
  return {
    // receipt,
    receiptItems,
    addReceiptItem,
    incUnitOfReceiptItem,
    decUnitOfReceiptItem,
    deleteReceiptItem,
    removeItem
  }
})
